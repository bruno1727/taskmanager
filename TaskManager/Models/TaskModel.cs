﻿namespace TaskManager.Models
{
    public class TaskModel
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
