﻿namespace TaskManager.Requests
{
    public class TaskRequest
    {
        public string Description { get; set; }
    }
}
